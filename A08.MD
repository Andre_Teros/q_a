#### 8.1. Принципы ООП
[Инкапсуляция] - механизм языка, позволяющий объединить данные и методы,
работающие с этими данными, в единый объект. А так же скрыть детали реализации

[Наследование] - механизм языка, позволяющий описывать новый класс на основе существующего

[Полиморфизм] - механизм языка, позволяющий использовать разные объекты с одинаковым интерфейсом

{Абстракция} - отделение концепции от ее экземпляра. Идея абстракции состоит в представлении
объекта с минимальным набором полей и методов и при этом достаточным для решения задачи

{Посылка сообщений}* - механизм языка, позволяющий одному объекту вызывать метод другого объекта

{Повторное использование}

* Пример php8
```php
Class A {
    public function getOne() {
        return 'one';
    }
}

Class B {
    public $g;

    public function __construct(
        public A $a
    ) {}
}

$a = new A();
$b = new B($a);
$b1 = null;

echo $b->a?->getOne();
echo $b->g?->getOne();
echo $b?->g?->getOne();

```

#### 8.2. SOLID

[SRP] A module should be responsible to one, and only one, actor. A module should have one, and only one, reason to change.
Пример: ActiveRecord


[OCP] Программные сущности должны быть открыты для расширения, но закрыты для модификации


[LSP] Методы, использующие некий тип, должны иметь возможность использовать любой подтип.
Объекты в программе могут быть заменены их наследниками без изменения свойств программы.
Предусловия не должны быть усилены, постусловия не должны быть ослаблены.

```php
Class A {}
Class B extends A {}
Class C extends B {}

Class Exception {}
Class RuntimeException extends Exception {}
Class CalculationException extends RuntimeException {}


Class Reader
{
    /**
     * @throws RuntimeException
     */
    public function read(B $arg): B {}
}

Class NewReader extends Reader
{
    /**
     * @throws CalculationException
     */
    public function read(A $arg): C {}
}
```


[ISP]
Много специализированных интерфейсов лучше, чем один универсальный


[DIP]
Модули верхних уровней не должны зависеть от модулей нижних уровней. Оба типа модулей должны зависеть от абстракций.
Абстракции не должны зависеть от деталей. Детали должны зависеть от абстракций.







#### 8.5.
Композиция - это ассоциация, при которой используемый объект создается внутри класса
```php
class Foo {}

class Bar {
    public function __construct() {
        $this->foo = new Foo();
    }
}
```

Агрегация - это ассоциация, при которой используемый объект создается вне класса
```php
class Foo {}

class Bar {
    public function __construct(Foo $foo) {
        $this->foo = $foo;
    }
}
```


#### Разные термины
Апкастинг позволяет рассматривать объект типа подкласса как объект любого типа суперкласса
```php
// пример

class Base {}
class Concrete extends Base {}

$c = new Concrete();
$c instanceof Base; // true

```
![class-interaction](https://gitlab.com/Andre_Teros/q_a/raw/master/img/class-interaction.jpg?raw=true "class-interaction")
