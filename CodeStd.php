<?php

/**
 * File-level docblock. Some description
 *
 * @author Name <author@mail.com>
 * @date 01.01.2020
 */

declare(strict_types=1);

namespace App\Pkg\Service;

use Ramsey\Uuid\Converter\NumberConverterInterface;
use Ramsey\Uuid\Converter\TimeConverterInterface;
use Vendor1\Services\BaseCodeStd;
use Vendor2\Contracts\StdIntercace;
use Vendor2\Contracts\YYYIntercace;
use Vendor2\Contracts\ZZZIntercace;
use Vendor3\Traits\Trait1;
use Vendor3\Traits\Trait2;

use function Vendor1\functions\func1;
use function Vendor1\functions\func2;

/**
 * Class CodeStd
 * @package App\Pkg\Service
 */
class CodeStd extends BaseCodeStd implements
    StdIntercace,
    YYYIntercace,
    ZZZIntercace
{
    use Trait1;
    use Trait2;

    public const FOO = 'FOO';
    protected const BAR = 'BAR';
    private const BAZ = 'BAZ';

    public float $sum = 0.0;
    protected NumberConverterInterfa $numberConverter;
    private TimeConverterInterface $timeConverter;

    /**
     * @param NumberConverterInterface $numberConverter The number converter to
     *     use when constructing the Guid
     * @param TimeConverterInterface $timeConverter The time converter to use
     *     for converting timestamps extracted from a UUID to Unix timestamps
     */
    public function __construct(
        NumberConverterInterface $numberConverter,
        TimeConverterInterface $timeConverter
    ) {
        $this->numberConverter = $numberConverter;
        $this->timeConverter = $timeConverter;
    }

    /**
     * Method description
     *
     * @param string $foo
     * @param string $bar
     * @param int $baz
     *
     * @return string
     * @throws \RuntimeException
     */
    public function anotherFunction(
        string $foo,
        string $bar,
        int $baz = 0
    ): ?string {

        somefunction($foo, $bar, [
            // ...
        ], $baz);

        if (
            strlen($foo) > 12
            && strlen($bar) <= 13
            && $baz !== 2
        ) {
            return null;
        }

        $floatValue = (float) $baz;

        $longArgs_longVars = function (
            $longArgument,
            $longerArgument,
            $muchLongerArgument
        ) use (
            $foo,
            $bar,
            $floatValue
        ) {
            // body
        };

        return 'foo';
    }
}
